package edu.umkc.jbvx8mail.digestiondetective;

import android.content.Context;
import android.test.AndroidTestCase;

import java.util.Date;
import java.util.List;

import edu.umkc.jbvx8mail.digestiondetective.meals.Food;
import edu.umkc.jbvx8mail.digestiondetective.meals.Meal;
import edu.umkc.jbvx8mail.digestiondetective.symptom.Symptom;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class DigestionDaoTest extends AndroidTestCase {
    private DigestionDAO digestionDAO;

    public void setUp() {
        Context c = getContext();
        digestionDAO = DigestionDAO.get(c);
    }

    public void testAddMeal() {
        Meal meal = new Meal();
        meal.setMealName("mealName");
        meal.setMealDate(new Date());
        long id = digestionDAO.addMeal(meal);
        List<Meal> meals = digestionDAO.getMeals();
        Meal actual = null;
        for (Meal m : meals) {
            if (id == m.getMealId()) {
                actual = m;
                break;
            }
        }

        assertEquals(meal.getMealDate(), actual.getMealDate());
        assertEquals(id, actual.getMealId());
        assertEquals(meal.getMealName(), actual.getMealName());
        //assertTrue("Could not insert meal into database", found);
        boolean deleteSuccessful = digestionDAO.deleteMeal(id);
        assertTrue("Could not delete inserted meal from database", deleteSuccessful);
    }

    public void testAddFood() {
        Food food = new Food();
        food.setFoodName("fruit");
        long id = digestionDAO.addFood(food);
        Food actual = digestionDAO.getFood(food.getFoodName());
        assertEquals(id, actual.getFoodId());

        boolean deleteSuccessful = digestionDAO.deleteFood(food);
        assertTrue("Could not delete inserted food from database", deleteSuccessful);
    }

    public void testAddSymptom() {
        Symptom symptom = new Symptom();
        symptom.setSymptomName("bloat");
        long id = digestionDAO.addSymptom(symptom);
        Symptom actual = digestionDAO.getSymptom("bloat");
        assertEquals(symptom.getSymptomName(), actual.getSymptomName());

        boolean deleteSuccessful = digestionDAO.deleteSymptom(symptom.getSymptomName());
        assertTrue("Could not delete inserted symptom from database", deleteSuccessful);
    }
}