package edu.umkc.jbvx8mail.digestiondetective;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import edu.umkc.jbvx8mail.digestiondetective.database.DetectiveDbHelper;
import edu.umkc.jbvx8mail.digestiondetective.database.MealCursorWrapper;
import edu.umkc.jbvx8mail.digestiondetective.database.SymptomLogCursorWrapper;
import edu.umkc.jbvx8mail.digestiondetective.meals.Food;
import edu.umkc.jbvx8mail.digestiondetective.meals.Meal;
import edu.umkc.jbvx8mail.digestiondetective.symptom.Symptom;
import edu.umkc.jbvx8mail.digestiondetective.symptom.SymptomLog;

import static edu.umkc.jbvx8mail.digestiondetective.database.DetectiveDbSchema.*;


public class DigestionDAO {
    private static DigestionDAO digestionDAO;
    private SQLiteDatabase database;

    public static DigestionDAO get(Context context) {
        if (digestionDAO == null) {
            digestionDAO = new DigestionDAO(context);
        }
        return digestionDAO;
    }

    private DigestionDAO(Context context) {
        database = new DetectiveDbHelper(context).getWritableDatabase();
    }

    public Meal getMeal(long mealId) {
        try (MealCursorWrapper cursor = queryMeals(MealTable.Cols.MEAL_ID + " = ?",
                new String[]{
                        Long.toString(mealId)})) {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getMeal();
        }
    }

    public long updateMeal(Meal meal) {
        String mealIDString = Long.toString(meal.getMealId());
        ContentValues values = getContentValues(meal);
        if (meal.getMealId() == 0) {
            return database.insert(MealTable.MEAL_TABLE_NAME, null, values);
        } else {
            database.update(MealTable.MEAL_TABLE_NAME, values, MealTable.Cols.MEAL_ID + " = ?",
                    new String[]{mealIDString});
        }
        return meal.getMealId();
    }

    public void ensureLog(SymptomLog log) {
        String logIDString = Long.toString(log.getLogId());
        ContentValues values = getContentValues(log);
        if (log.getLogId() == 0) {
            database.insert(UserSymptoms.USER_SYMPTOMS_TABLE_NAME, null, values);
        } else {
            database.update(UserSymptoms.USER_SYMPTOMS_TABLE_NAME, values, UserSymptoms.Cols.USER_SYMPTOM_ID + " = ?",
                    new String[]{logIDString});
        }
    }

    public void deleteMeal(Meal meal) {
        String mealIDString = Long.toString(meal.getMealId());
        database.delete(MealTable.MEAL_TABLE_NAME, MealTable.Cols.MEAL_ID + " = ?", new String[]{mealIDString});
    }

    public void deleteSymptomLog(SymptomLog log) {
        String logIdString = Long.toString(log.getLogId());
        database.delete(UserSymptoms.USER_SYMPTOMS_TABLE_NAME, UserSymptoms.Cols.USER_SYMPTOM_ID + " = ?", new String[]{logIdString});
    }

    private static ContentValues getContentValues(Meal meal) {
        ContentValues values = new ContentValues();
        if (meal.getMealId() != 0) {
            values.put(MealTable.Cols.MEAL_ID, Long.toString(meal.getMealId()));
        }
        values.put(MealTable.Cols.MEAL_NAME, meal.getMealName());
        values.put(MealTable.Cols.MEAL_DATE, meal.getMealDate().getTime());

        return values;
    }

    private static ContentValues getContentValues(SymptomLog log) {
        ContentValues values = new ContentValues();
        if (log.getLogId() != 0) {
            values.put(UserSymptoms.Cols.USER_SYMPTOM_ID, Long.toString(log.getLogId()));
        }
        values.put(UserSymptoms.Cols.USER_NOTES, log.getSymptomNotes());
        values.put(UserSymptoms.Cols.USER_SYMPTOM_DATE, log.getSymptomDate().getTime());
        values.put(UserSymptoms.Cols.SYMPTOM_ID, log.getSymptom().getSymptomId());


        return values;
    }


    private MealCursorWrapper queryMeals(String whereClause, String[] whereArgs) {
            Cursor cursor = database.query(MealTable.MEAL_TABLE_NAME, null, whereClause, whereArgs, null, null, null);
            return new MealCursorWrapper(cursor);
    }

    private SymptomLogCursorWrapper queryLog(String whereClause, String[] whereArgs) {
        Cursor cursor = database.query(UserSymptoms.USER_SYMPTOMS_TABLE_NAME, null, whereClause, whereArgs, null, null, null);
        return new SymptomLogCursorWrapper(cursor);
    }

    public long addMeal(Meal meal) {
        ContentValues values = new ContentValues();
        values.put(MealTable.Cols.MEAL_NAME, meal.getMealName());
        values.put(MealTable.Cols.MEAL_DATE, meal.getMealDate().getTime());
        return database.insert(MealTable.MEAL_TABLE_NAME, null, values);
    }

    public long addMealFoods(long mealId, int foodId) {
        ContentValues values = new ContentValues();
        values.put(MealFoods.Cols.MEAL_ID, mealId);
        values.put(MealFoods.Cols.FOOD_ID, foodId);
        return database.insert(MealFoods.MEALFOODS_TABLE_NAME, null, values);
    }

    public long addFood(Food food) {
        ContentValues values = new ContentValues();
        values.put(FoodTable.Cols.FOOD_NAME, food.getFoodName());
        return database.insert(FoodTable.FOOD_TABLE_NAME, null, values);
    }

    public boolean deleteFood(Food food) {
        try {
            database.delete(FoodTable.FOOD_TABLE_NAME, FoodTable.Cols.FOOD_ID + " = ?", new String[]{Long.toString(food.getFoodId())});
            return true;
        } catch (SQLException e) {
            Log.i("DigestionDAO", "Couldn't delete food\n" + e);
            return false;
        }
    }

    public long addSymptom(Symptom symptom) {
        ContentValues values = new ContentValues();
        values.put(SymptomsTable.Cols.SYMPTOM_NAME, symptom.getSymptomName());
        return database.insert(SymptomsTable.SYMPTOMS_TABLE_NAME, null, values);
    }

    public boolean deleteSymptom(String name) {
        try {
            database.delete(SymptomsTable.SYMPTOMS_TABLE_NAME, SymptomsTable.Cols.SYMPTOM_NAME + " = ?", new String[]{name});
            return true;
        } catch (SQLException e) {
            Log.i("DigestionDAO", "Couldn't delete symptom\n" + e);
            return false;
        }
    }

    public boolean deleteMeal(long mealId) {
        try {
            database.delete(MealTable.MEAL_TABLE_NAME, MealTable.Cols.MEAL_ID + " = ?", new String[]{Long.toString(mealId)});
            return true;
        } catch (SQLException e) {
            Log.i("DigestionDAO", "Couldn't delete meal\n" + e);
            return false;
        }
    }

    public void deleteMealFoods(long mealId) {
        try {
            database.delete(MealFoods.MEALFOODS_TABLE_NAME, MealFoods.Cols.MEAL_ID + " = ?", new String[]{Long.toString(mealId)});
        } catch (SQLException e) {
            Log.i("DigestionDAO", "Couldn't delete meal\n" + e);
        }
    }

    public Food getFood(String foodName) {
        String selectQuery = "SELECT * FROM " + FoodTable.FOOD_TABLE_NAME + " WHERE " + FoodTable.Cols.FOOD_NAME + " = '" + foodName + "'";
        try {
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                Food food = new Food();
                food.setFoodName(cursor.getString(cursor.getColumnIndex(FoodTable.Cols.FOOD_NAME)));
                food.setFoodId(cursor.getInt(cursor.getColumnIndex(FoodTable.Cols.FOOD_ID)));
                cursor.close();
                return food;
            }
        } catch (SQLException e) {
            Log.i("DigestionDAO", "Could not get food from database\n" + e);
        }
        return null;
    }

    public Symptom getSymptom(String symptomName) {
        String selectQuery = "SELECT * FROM " + SymptomsTable.SYMPTOMS_TABLE_NAME + " WHERE " + SymptomsTable.Cols.SYMPTOM_NAME + " = '" + symptomName + "'";
        try {
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                Symptom symptom = new Symptom();
                symptom.setSymptomName(cursor.getString(cursor.getColumnIndex(SymptomsTable.Cols.SYMPTOM_NAME)));
                symptom.setSymptomId(cursor.getLong(cursor.getColumnIndex(SymptomsTable.Cols.SYMPTOM_ID)));
                cursor.close();
                return symptom;
            }
        } catch (SQLException e) {
            Log.i("DigestionDAO", "Could not get symptom from database\n" + e);
        }
        return null;
    }

    public String getSymptomNameFromLog(SymptomLog log) {
        Symptom symptom = log.getSymptom();
        if (symptom != null) {
            long symptomId = log.getSymptom().getSymptomId();
            String selectQuery = "SELECT * FROM " + SymptomsTable.SYMPTOMS_TABLE_NAME + " WHERE " + SymptomsTable.Cols.SYMPTOM_ID + " = '" + symptomId + "'";
            try {
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    return cursor.getString(cursor.getColumnIndex(SymptomsTable.Cols.SYMPTOM_NAME));
                }
                cursor.close();
            } catch (SQLException e) {
                Log.i("DigestionDAO", "Could not get symptom from database\n" + e);
            }
        }
        return null;
    }

    public List<SymptomLog> getUserSymptoms() {
        List<SymptomLog> logList = new ArrayList<>();
        try (SymptomLogCursorWrapper cursor = queryLog(null, null)) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                logList.add(cursor.getUserSymptom());
                cursor.moveToNext();
            }
        }
        for (SymptomLog log : logList) {
            Symptom symptom = log.getSymptom();
            if (symptom != null) {
                symptom.setSymptomName(getSymptomNameFromLog(log));
            }
        }
        return logList;
    }


    public List<Food> getAllFoods() {
        List<Food> foods = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + FoodTable.FOOD_TABLE_NAME;
        try (Cursor cursor = database.rawQuery(selectQuery, null)) {
            if (cursor.moveToFirst()) {
                do {
                    Food food = new Food();
                    food.setFoodName(cursor.getString(cursor.getColumnIndex(FoodTable.Cols.FOOD_NAME)));
                    food.setFoodId(cursor.getInt(cursor.getColumnIndex(FoodTable.Cols.FOOD_ID)));
                    foods.add(food);
                } while (cursor.moveToNext());
            }
        } catch (SQLException e) {
            Log.i("DigestionDAO", "Could not get foods from database\n" + e);
        }
        return foods;
    }

    public List<Food> getFoodsByMeal(long mealId) {
        List<Food> foods = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + MealTable.MEAL_TABLE_NAME + " m, "
                + FoodTable.FOOD_TABLE_NAME + " f, " + MealFoods.MEALFOODS_TABLE_NAME + " mf WHERE "
                + " m." + MealTable.Cols.MEAL_ID + " = " + mealId + " AND "
                + " m." + MealTable.Cols.MEAL_ID + " = mf." + MealFoods.Cols.MEAL_ID + " AND "
                + " f." + FoodTable.Cols.FOOD_ID + " = mf." + MealFoods.Cols.FOOD_ID;

        try (Cursor cursor = database.rawQuery(selectQuery, null)) {

            if (cursor.moveToFirst()) {
                do {
                    Food food = new Food();
                    food.setFoodId(cursor.getInt(cursor.getColumnIndex(FoodTable.Cols.FOOD_ID)));
                    food.setFoodName(cursor.getString(cursor.getColumnIndex(FoodTable.Cols.FOOD_NAME)));
                    foods.add(food);
                } while (cursor.moveToNext());
            }
        } catch (SQLException e) {
            Log.i("DigestionDAO", "Could not get foods by meal from database\n" + e);
        }
        return foods;
    }

    public List<Meal> getMeals() {
        List<Meal> meals = new ArrayList<>();
        try (MealCursorWrapper cursor = queryMeals(null, null)) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                meals.add(cursor.getMeal());
                cursor.moveToNext();
            }
        }
        return meals;
    }


    public List<Meal> getMealsByDate(Date date) {
        List<Meal> meals = new ArrayList<>();
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long day = 24 * 60 * 60 * 1000;
        String selectQuery = "SELECT * FROM " + MealTable.MEAL_TABLE_NAME + " WHERE "
                + MealTable.Cols.MEAL_DATE + " >= " + (date.getTime() - day + " AND " + MealTable.Cols.MEAL_DATE + " <= " + date.getTime());
        try {
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Meal meal = new Meal();
                    meal.setMealId(cursor.getLong(cursor.getColumnIndex(MealTable.Cols.MEAL_ID)));
                    meal.setMealName(cursor.getString(cursor.getColumnIndex(MealTable.Cols.MEAL_NAME)));
                    long mealDate = cursor.getLong(cursor.getColumnIndex(MealTable.Cols.MEAL_DATE));
                    meal.setMealDate(new Date(mealDate));
                    meals.add(meal);
                } while (cursor.moveToNext());
            }

            cursor.close();
        } catch (SQLException e) {
            Log.i("DigestionDAO", "Could not get meals from database by date\n" + e);
        }
        return meals;
    }

    public Map<String, Integer> getFoodsByDate(Date date) {
        Map<String, Integer> foodMap = new HashMap<>();
        List<Meal> meals = getMealsByDate(date);

        for(int i = 0; i < meals.size(); i++) {
            List<Food> foods = getFoodsByMeal(meals.get(i).getMealId());
            for (int j = 0; j < foods.size(); j++) {
                String foodName = foods.get(j).getFoodName();
                if (foodMap.get(foodName) == null) {
                    foodMap.put(foodName, 1);
                }
                else {
                    Integer foodCount = foodMap.get(foodName);
                    foodMap.put(foodName, foodCount + 1);
                }
            }
        }
        return foodMap;
    }

    public List<Date> getDatesOfSymptom(String symptom) {
        List<Date> dates = new ArrayList<>();
        String selectQuery = "SELECT " + UserSymptoms.Cols.USER_SYMPTOM_DATE + " FROM " + UserSymptoms.USER_SYMPTOMS_TABLE_NAME + " us INNER JOIN " + SymptomsTable.SYMPTOMS_TABLE_NAME +
                " s ON us." + UserSymptoms.Cols.SYMPTOM_ID + " = s." + SymptomsTable.Cols.SYMPTOM_ID +
                " WHERE s." + SymptomsTable.Cols.SYMPTOM_NAME + " = '" + symptom + "'";

        try {
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    long symptomDate = cursor.getLong(cursor.getColumnIndex(UserSymptoms.Cols.USER_SYMPTOM_DATE));
                    Date date = new Date(symptomDate);
                    dates.add(date);
                } while (cursor.moveToNext());
            }

            cursor.close();
        } catch (SQLException e) {
            Log.i("DigestionDAO", "Could not get meals from database by date\n" + e);
        }
        return dates;
    }

    public int getCountOfSymptom(String symptom) {
        int count = 0;

        String selectQuery = "SELECT " + UserSymptoms.Cols.USER_SYMPTOM_DATE + " FROM " + UserSymptoms.USER_SYMPTOMS_TABLE_NAME + " us INNER JOIN " + SymptomsTable.SYMPTOMS_TABLE_NAME +
                " s ON us." + UserSymptoms.Cols.SYMPTOM_ID + " = s." + SymptomsTable.Cols.SYMPTOM_ID +
                " WHERE s." + SymptomsTable.Cols.SYMPTOM_NAME + " = '" + symptom + "'";

        try {
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    count++;
                } while (cursor.moveToNext());
            }

            cursor.close();
        } catch (SQLException e) {
            Log.i("DigestionDAO", "Could not get a count of symptom from database\n" + e);
        }

        return count;
    }

    public Map<String, Integer> getFoodsNotWithSymptom (String symptom) {
        List<Date> symptomDates = getDatesOfSymptom(symptom);
        List<Meal> mealsWithinDate = new ArrayList<>();
        Map<String, Integer> foodMap = new HashMap<>();

        for (int i = 0; i < symptomDates.size(); i++) {
            List<Meal> mealList = getMealsByDate(symptomDates.get(i));
            mealsWithinDate.addAll(mealList);
        }

        List<Meal> mealsOutsideDate = getMeals();
        mealsOutsideDate.removeAll(mealsWithinDate);

        for (int j = 0; j < mealsOutsideDate.size(); j++) {
            List<Food> mealFoods = getFoodsByMeal(mealsOutsideDate.get(j).getMealId());
            for (int k = 0; k < mealFoods.size(); k++) {
                incrementFoodMap(foodMap, mealFoods.get(k).getFoodName());
            }
        }
        return foodMap;
    }

    public void incrementFoodMap(Map<String, Integer> mapToIncrement, String key) {
        if (mapToIncrement.containsKey(key)) {
            mapToIncrement.put(key, mapToIncrement.get(key) + 1);
        }
        else {
            mapToIncrement.put(key, 1);
        }
    }

    public Map<String, Integer> getFoodsBySymptom(String symptom) {
        Map<String, Integer> foodMap = new HashMap<>();
        List<Date> dates = getDatesOfSymptom(symptom);
        for (int i = 0; i < dates.size(); i++) {
            Map<String, Integer> foodMapByDate = getFoodsByDate(dates.get(i));
            for(Map.Entry<String, Integer> entry : foodMapByDate.entrySet()) {
                switch (entry.getKey()) {
                    case "soy":
                        incrementFoodMap(foodMap, "soy");
                        break;
                    case "dairy":
                        incrementFoodMap(foodMap, "dairy");
                        break;
                    case "wheat":
                        incrementFoodMap(foodMap, "wheat");
                        break;
                    case "caffeine":
                        incrementFoodMap(foodMap, "caffeine");
                        break;
                    case "bagged produce":
                        incrementFoodMap(foodMap, "bagged produce");
                        break;
                    case "beans":
                        incrementFoodMap(foodMap, "beans");
                        break;
                    case "high fiber":
                        incrementFoodMap(foodMap, "high fiber");
                        break;
                    case "spicy foods":
                        incrementFoodMap(foodMap, "spicy foods");
                        break;
                }
            }
        }
        return foodMap;
    }



    public int getSymptomCount() {
        String selectQuery = "SELECT COUNT(*) FROM " + SymptomsTable.SYMPTOMS_TABLE_NAME;
        Cursor cursor = database.rawQuery(selectQuery, null);

        int count = 0;
        if (cursor.moveToFirst()) {
            count = cursor.getInt(0);
        }
        cursor.close();
        return count;
    }

    public SymptomLog getUserSymptom(long logId) {

        try (SymptomLogCursorWrapper cursor = queryLog(UserSymptoms.Cols.USER_SYMPTOM_ID + " = ?",
                new String[]{
                        Long.toString(logId)})) {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();

            SymptomLog log = cursor.getUserSymptom();
            Symptom symptom = log.getSymptom();
            if (symptom != null) {
                symptom.setSymptomName(getSymptomNameFromLog(log));
            }
            return log;
        }
    }

    public void closeDb() {
        if (database != null && database.isOpen()){
            database.close();
        }
    }

}
