package edu.umkc.jbvx8mail.digestiondetective.analysis;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.Map;

import edu.umkc.jbvx8mail.digestiondetective.DigestionDAO;
import edu.umkc.jbvx8mail.digestiondetective.R;
import edu.umkc.jbvx8mail.digestiondetective.symptom.Symptom;


public class AnalysisFragment extends Fragment {

    private Symptom symptom = new Symptom();
    private DigestionDAO digestionDao;
    private Button okButton;
    private RadioButton heartburnButton, abdominalButton, nauseaButton, hivesButton, constipationButton, looseButton, hardButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        digestionDao = DigestionDAO.get(getActivity());
    }


    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_analysis_chooser, container, false);

        heartburnButton = (RadioButton) v.findViewById(R.id.radioHeartburn);
        heartburnButton.setOnClickListener(radioListener);

        abdominalButton = (RadioButton) v.findViewById(R.id.radioAbdominalPain);
        abdominalButton.setOnClickListener(radioListener);

        hivesButton = (RadioButton) v.findViewById(R.id.radioHives);
        hivesButton.setOnClickListener(radioListener);

        constipationButton = (RadioButton) v.findViewById(R.id.radioConstipation);
        constipationButton.setOnClickListener(radioListener);

        nauseaButton = (RadioButton) v.findViewById(R.id.radioNausea);
        nauseaButton.setOnClickListener(radioListener);

        looseButton = (RadioButton) v.findViewById(R.id.radioLoose);
        looseButton.setOnClickListener(radioListener);

        hardButton = (RadioButton) v.findViewById(R.id.radioHard);
        hardButton.setOnClickListener(radioListener);

        if (symptom.getSymptomId() != 0) {
            setCheckedRadioButton();
        }


        okButton = (Button)v.findViewById(R.id.analyze_symptom_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View resultView = inflater.inflate(R.layout.dialog_results, null);
                    AlertDialog dialog = new AlertDialog.Builder(getActivity())
                            .setView(resultView)
                            .setPositiveButton(android.R.string.ok, null)
                            .create();
                TextView textView = (TextView) resultView.findViewById(R.id.result_heading1);
                if (symptom.getSymptomName() != null) {
                    displayResults(resultView);
                }
                else {
                    textView.setText(R.string.dialog_results_no_symptom);
                }
                dialog.show();
            }
        });
        return v;
    }

    public void setTextVisibility(final Context context, final View v) {
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                View child = vg.getChildAt(i);
                setTextVisibility(context, child);
            }
        } else if (v instanceof TextView ) {
            if (((TextView) v).getText() == null || "".equals(((TextView) v).getText())) {
                ((TextView) v).setVisibility(View.GONE);
            }
        }
    }

    public void displayResults(View resultView) {
        int count = digestionDao.getCountOfSymptom(symptom.getSymptomName());
        TextView tv = new TextView(getContext());
        boolean withSymptom = false;
        Resources res = getResources();
        Map<String, Integer> foodsWithSymptom = digestionDao.getFoodsBySymptom(symptom.getSymptomName());
        TextView heading = (TextView) resultView.findViewById(R.id.result_heading1);
        heading.setText(res.getQuantityString(R.plurals.dialog_results_heading1, count, symptom.getSymptomName(), count));

        for (Map.Entry<String, Integer> entry : foodsWithSymptom.entrySet()) {
            int number = entry.getValue();
            switch (entry.getKey()) {
                case "dairy":
                    tv = (TextView) resultView.findViewById(R.id.result_dairy_with_symptom);
                    tv.setText(getString(R.string.dialog_results_heading2, "dairy", number, count));
                    withSymptom = true;
                    break;
                case "soy":
                    tv = (TextView) resultView.findViewById(R.id.result_soy_with_symptom);
                    tv.setText(getString(R.string.dialog_results_heading2, "soy", number, count));
                    withSymptom = true;
                    break;
                case "wheat":
                    tv = (TextView) resultView.findViewById(R.id.result_wheat_with_symptom);
                    tv.setText(getString(R.string.dialog_results_heading2, "wheat", number, count));
                    withSymptom = true;
                    break;
                case "caffeine":
                    tv = (TextView) resultView.findViewById(R.id.result_caffeine_with_symptom);
                    tv.setText(getString(R.string.dialog_results_heading2, "caffeine", number, count));
                    withSymptom = true;
                    break;
                case "bagged produce":
                    tv = (TextView) resultView.findViewById(R.id.result_bagged_produce_with_symptom);
                    tv.setText(getString(R.string.dialog_results_heading2, "bagged produce", number, count));
                    withSymptom = true;
                    break;
                case "beans":
                    tv = (TextView) resultView.findViewById(R.id.result_beans_with_symptom);
                    tv.setText(getString(R.string.dialog_results_heading2, "beans", number, count));
                    withSymptom = true;
                    break;
                case "high fiber":
                    tv = (TextView) resultView.findViewById(R.id.result_high_fiber_with_symptom);
                    tv.setText(getString(R.string.dialog_results_heading2, "high fiber", number, count));
                    withSymptom = true;
                    break;
                case "spicy foods":
                    tv = (TextView) resultView.findViewById(R.id.result_spicy_foods_with_symptom);
                    tv.setText(getString(R.string.dialog_results_heading2, "spicy foods", number, count));
                    withSymptom = true;
                    break;
            }
        }

        if (!withSymptom) {
            TextView noResult = (TextView) resultView.findViewById(R.id.result_none);
            noResult.setText(R.string.dialog_results_none);
        }

        TextView heading3 = (TextView) resultView.findViewById(R.id.result_heading2);
        heading3.setText(getString(R.string.dialog_results_heading3, symptom.getSymptomName()));

        boolean withoutSymptom = false;
        Map<String, Integer> foodsWithoutSymptom = digestionDao.getFoodsNotWithSymptom(symptom.getSymptomName());
        for (Map.Entry<String, Integer> entry : foodsWithoutSymptom.entrySet()) {
            int number = entry.getValue();
            switch (entry.getKey()) {
                case "dairy":
                    tv = (TextView) resultView.findViewById(R.id.result_dairy_without_symptom);
                    tv.setText(res.getQuantityString(R.plurals.dialog_results_label, number, "dairy", number));
                    withoutSymptom = true;
                    break;
                case "soy":
                    tv = (TextView) resultView.findViewById(R.id.result_soy_without_symptom);
                    tv.setText(res.getQuantityString(R.plurals.dialog_results_label, number, "soy", number));
                    withoutSymptom = true;
                    break;
                case "wheat":
                    tv = (TextView) resultView.findViewById(R.id.result_wheat_without_symptom);
                    tv.setText(res.getQuantityString(R.plurals.dialog_results_label, number, "wheat", number));
                    withoutSymptom = true;
                    break;
                case "caffeine":
                    tv = (TextView) resultView.findViewById(R.id.result_caffeine_without_symptom);
                    tv.setText(res.getQuantityString(R.plurals.dialog_results_label, number, "caffeine", number));
                    withoutSymptom = true;
                    break;
                case "bagged produce":
                    tv = (TextView) resultView.findViewById(R.id.result_bagged_produce_without_symptom);
                    tv.setText(res.getQuantityString(R.plurals.dialog_results_label, number, "bagged produce", number));
                    withoutSymptom = true;
                    break;
                case "beans":
                    tv = (TextView) resultView.findViewById(R.id.result_beans_without_symptom);
                    tv.setText(res.getQuantityString(R.plurals.dialog_results_label, number, "beans", number));
                    withoutSymptom = true;
                    break;
                case "high fiber":
                    tv = (TextView) resultView.findViewById(R.id.result_high_fiber_without_symptom);
                    tv.setText(res.getQuantityString(R.plurals.dialog_results_label, number, "high fiber", number));
                    withoutSymptom = true;
                    break;
                case "spicy foods":
                    tv = (TextView) resultView.findViewById(R.id.result_spicy_foods_without_symptom);
                    tv.setText(res.getQuantityString(R.plurals.dialog_results_label, number, "spicy foods", number));
                    withoutSymptom = true;
                    break;
            }
        }

        TextView heading4 = (TextView) resultView.findViewById(R.id.result_heading3);
        heading4.setText(R.string.dialog_results_heading4);
        if (!withoutSymptom) {
            TextView noResult = (TextView) resultView.findViewById(R.id.result_nothing_without);
            noResult.setText(R.string.dialog_results_none);
        }
        TextView heading5 = (TextView) resultView.findViewById(R.id.result_heading4);
        heading5.setText(getString(R.string.dialog_results_heading5, symptom.getSymptomName()));

        setTextVisibility(getContext(), resultView);
    }


    public View.OnClickListener radioListener = new View.OnClickListener() {
        public void onClick(View view) {
            boolean checked = ((RadioButton) view).isChecked();
            switch (view.getId()) {
                case R.id.radioHeartburn:
                    if (checked) {
                        symptom.setSymptomName("heartburn");
                    }
                    break;
                case R.id.radioAbdominalPain:
                    if (checked) {
                        symptom.setSymptomName("abdominal pain");
                    }
                    break;
                case R.id.radioHives:
                    if (checked) {
                        symptom.setSymptomName("hives");
                    }
                    break;
                case R.id.radioNausea:
                    if (checked) {
                        symptom.setSymptomName("nausea");
                    }
                    break;
                case R.id.radioConstipation:
                    if (checked) {
                        symptom.setSymptomName("constipation");
                    }
                    break;
                case R.id.radioLoose:
                    if (checked) {
                        symptom.setSymptomName("loose stool");
                    }
                    break;
                case R.id.radioHard:
                    if (checked) {
                        symptom.setSymptomName("hard stool");
                    }
                    break;

            }
        }
    };

    public void setCheckedRadioButton() {
        switch (symptom.getSymptomName()) {
            case "heartburn":
                heartburnButton.setChecked(true); break;
            case "nausea":
                nauseaButton.setChecked(true); break;
            case "hives":
                hivesButton.setChecked(true); break;
            case "abdominal pain":
                abdominalButton.setChecked(true); break;
            case "constipation":
                constipationButton.setChecked(true); break;
            case "loose stool":
                looseButton.setChecked(true); break;
            case "hard stool":
                hardButton.setChecked(true); break;
        }
    }
}
