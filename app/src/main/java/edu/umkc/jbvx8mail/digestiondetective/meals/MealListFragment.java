package edu.umkc.jbvx8mail.digestiondetective.meals;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.umkc.jbvx8mail.digestiondetective.DigestionDAO;
import edu.umkc.jbvx8mail.digestiondetective.R;


public class MealListFragment extends Fragment {

    private RecyclerView mealRecyclerView;
    private MealAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        if (view == null) throw new AssertionError("Meal List view is null");
        mealRecyclerView = (RecyclerView) view
                .findViewById(R.id.recycler_view);
        mealRecyclerView.setLayoutManager((new LinearLayoutManager(getActivity())));

        updateUI();

        return view;
    }

    public  void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_meal_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_meal:
                Intent intent = MealPagerActivity.newIntent(getActivity(), 0);
                startActivity(intent);
                return true;
            default:return super.onOptionsItemSelected(item);
        }

    }

    private void updateUI() {
        DigestionDAO digestionDAO = DigestionDAO.get(getActivity());
        List<Meal> mealList = digestionDAO.getMeals();
        if (adapter == null)
        {
            adapter = new MealAdapter(mealList);
            mealRecyclerView.setAdapter(adapter);
        }
        else {
            adapter.setMealList(mealList);
            adapter.notifyDataSetChanged();
        }

    }

    private class MealHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mealNameTextView;
        private TextView mealDateTextView;
        private TextView mealFoodsTextView;



        private Meal meal;
        public MealHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mealNameTextView = (TextView) itemView.findViewById(R.id.list_item_meal_name_text_view);
            mealDateTextView = (TextView) itemView.findViewById(R.id.list_item_meal_date_text_view);
            mealFoodsTextView = (TextView) itemView.findViewById(R.id.list_item_meal_food_list_text_view);
        }

        @Override
        public void onClick(View v) {
            Intent intent = MealPagerActivity.newIntent(getActivity(), meal.getMealId());
            startActivity(intent);
        }

        public void bindMeal(Meal m) {
            meal = m;
            mealNameTextView.setText(meal.getMealName());
            mealDateTextView.setText(meal.getMealDate().toString());
            DigestionDAO digestionDAO = DigestionDAO.get(getActivity());
            List<Food> foodList = digestionDAO.getFoodsByMeal(meal.getMealId());

            mealFoodsTextView.setText(foodListToString(foodList));
        }

        public String foodListToString(List<Food> foodList) {
            String foods = "";
            if (foodList == null) {
                foodList = new ArrayList<>();
            }
            for (int i = 0; i < foodList.size(); i++) {
                if (i < foodList.size()-1){
                    foods += foodList.get(i).getFoodName() + ", ";
                }
                else {
                    foods += foodList.get(i).getFoodName();
                }
            }
            return foods;
        }
    }



    private class MealAdapter extends RecyclerView.Adapter<MealHolder> {
        private List<Meal> mealList;
        public MealAdapter(List<Meal> meals) {
            mealList = meals;
        }

        @Override
        public MealHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater
                    .inflate(R.layout.list_item_meal, parent, false);
            return new MealHolder(view);
        }

        @Override
        public void onBindViewHolder(MealHolder holder, int position) {
            Meal meal = mealList.get(position);
            holder.bindMeal(meal);
        }

        @Override
        public int getItemCount() {
            return mealList.size();
        }

        public void setMealList(List<Meal> meals) {
            mealList = meals;
        }
    }
}
