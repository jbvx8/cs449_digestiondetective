package edu.umkc.jbvx8mail.digestiondetective.meals;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import edu.umkc.jbvx8mail.digestiondetective.R;

public class RecentMealsFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity()).setTitle(R.string.dialog_title_recent_meals)
                .setView(R.layout.dialog_recent_meals)
                .setPositiveButton(android.R.string.ok, null)
                .create();
    }
}
