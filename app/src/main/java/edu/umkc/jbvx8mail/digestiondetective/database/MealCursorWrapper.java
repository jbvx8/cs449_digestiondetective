package edu.umkc.jbvx8mail.digestiondetective.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.Date;

import edu.umkc.jbvx8mail.digestiondetective.meals.Meal;

import static edu.umkc.jbvx8mail.digestiondetective.database.DetectiveDbSchema.*;

/**
 * Created by AnonymousII on 2/28/2016.
 */
public class MealCursorWrapper extends CursorWrapper {
    public MealCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Meal getMeal() {
        long mealId = getLong(getColumnIndex(MealTable.Cols.MEAL_ID));
        String mealName = getString(getColumnIndex(MealTable.Cols.MEAL_NAME));
        long mealDate = getLong(getColumnIndex(MealTable.Cols.MEAL_DATE));

        Meal meal = new Meal(mealId, mealName);
        meal.setMealName(mealName);
        meal.setMealDate(new Date(mealDate));

        return meal;
    }

}
