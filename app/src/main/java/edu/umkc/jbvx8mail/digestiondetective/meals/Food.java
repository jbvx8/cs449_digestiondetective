package edu.umkc.jbvx8mail.digestiondetective.meals;


public class Food {
    private int foodId;
    private String foodName;

    public Food() {

    }

    public Food(String foodName) {
        this.foodName = foodName;
    }

    public int getFoodId() {
        return foodId;
    }


    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }
}
