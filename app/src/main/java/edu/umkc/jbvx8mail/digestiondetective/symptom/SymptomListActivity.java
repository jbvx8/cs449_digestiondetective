package edu.umkc.jbvx8mail.digestiondetective.symptom;

import android.support.v4.app.Fragment;

import edu.umkc.jbvx8mail.digestiondetective.SingleFragmentActivity;

/**
 * Created by AnonymousII on 3/20/2016.
 */
public class SymptomListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new SymptomListFragment();
    }
}
