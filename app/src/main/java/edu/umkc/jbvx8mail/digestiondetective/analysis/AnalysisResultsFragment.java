package edu.umkc.jbvx8mail.digestiondetective.analysis;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import edu.umkc.jbvx8mail.digestiondetective.R;

public class AnalysisResultsFragment extends DialogFragment{
    @Override
    public Dialog onCreateDialog(Bundle SavedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setView(R.layout.dialog_results)
                .setPositiveButton(android.R.string.ok, null)
                .create();
    }

}
