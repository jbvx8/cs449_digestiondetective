package edu.umkc.jbvx8mail.digestiondetective.symptom;

import java.util.Date;


public class SymptomLog {
    private long logId;
    private Symptom symptom;
    private Date symptomDate;
    private String symptomNotes;

    public SymptomLog() {
        symptomDate = new Date();
        symptom = new Symptom();
    }

    public SymptomLog(long id, String notes) {
        logId = id;
        symptomNotes = notes;
        symptomDate = new Date();
    }

    public long getLogId() {
        return logId;
    }

    public Symptom getSymptom() {
        return symptom;
    }

    public void setSymptom(Symptom symptom) {
        this.symptom = symptom;
    }

    public Date getSymptomDate() {
        return symptomDate;
    }

    public void setSymptomDate(Date symptomDate) {
        this.symptomDate = symptomDate;
    }

    public String getSymptomNotes() {
        return symptomNotes;
    }

    public void setSymptomNotes(String symptomNotes) {
        this.symptomNotes = symptomNotes;
    }
}
