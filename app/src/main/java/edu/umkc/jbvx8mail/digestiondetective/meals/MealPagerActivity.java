package edu.umkc.jbvx8mail.digestiondetective.meals;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import edu.umkc.jbvx8mail.digestiondetective.DigestionDAO;
import edu.umkc.jbvx8mail.digestiondetective.R;


public class MealPagerActivity extends AppCompatActivity {
    private static final String EXTRA_MEAL_ID = "edu.umkc.jbvx8mail.digestiondetective.meal_id";
    private ViewPager viewPager;
    private List<Meal> mealList;

    public static Intent newIntent(Context packageContext, long mealId) {
        Intent intent = new Intent(packageContext, MealPagerActivity.class);
        intent.putExtra(EXTRA_MEAL_ID, mealId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);
        viewPager = (ViewPager) findViewById(R.id.activity_pager_view_pager);
        long mealId = (long) getIntent().getSerializableExtra(EXTRA_MEAL_ID);

        mealList = DigestionDAO.get(this).getMeals();
        if (mealId == 0) {
            mealList.add(new Meal());
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Meal meal = mealList.get(position);
                return MealFragment.newInstance(meal.getMealId());
            }

            @Override
            public int getCount() {
                return mealList.size();
            }
        });

        for (int i = 0; i < mealList.size(); i++) {
            if (mealList.get(i).getMealId() == mealId) {
                viewPager.setCurrentItem(i);
                break;
            }
        }

    }
}
