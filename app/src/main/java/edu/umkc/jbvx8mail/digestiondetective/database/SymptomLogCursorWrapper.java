package edu.umkc.jbvx8mail.digestiondetective.database;


import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.Date;

import edu.umkc.jbvx8mail.digestiondetective.symptom.Symptom;
import edu.umkc.jbvx8mail.digestiondetective.symptom.SymptomLog;

public class SymptomLogCursorWrapper extends CursorWrapper{
    public SymptomLogCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public SymptomLog getUserSymptom() {
        long logId = getLong(getColumnIndex(DetectiveDbSchema.UserSymptoms.Cols.USER_SYMPTOM_ID));
        String symptomNotes = getString(getColumnIndex(DetectiveDbSchema.UserSymptoms.Cols.USER_NOTES));
        long symptomDate = getLong(getColumnIndex(DetectiveDbSchema.UserSymptoms.Cols.USER_SYMPTOM_DATE));

        SymptomLog log = new SymptomLog(logId, symptomNotes);
        log.setSymptomNotes(symptomNotes);
        log.setSymptomDate(new Date(symptomDate));

        Symptom symptom = new Symptom();
        symptom.setSymptomId(getLong(getColumnIndex(DetectiveDbSchema.UserSymptoms.Cols.SYMPTOM_ID)));
        log.setSymptom(symptom);
        return log;
    }
}
