package edu.umkc.jbvx8mail.digestiondetective.meals;

import android.support.v4.app.Fragment;

import edu.umkc.jbvx8mail.digestiondetective.SingleFragmentActivity;


public class MealListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new MealListFragment();
    }
}
