package edu.umkc.jbvx8mail.digestiondetective.database;

import static edu.umkc.jbvx8mail.digestiondetective.database.DetectiveDbSchema.SymptomsTable.SYMPTOMS_TABLE_NAME;


public class DetectiveDbSchema {
    public static final class MealTable {
        public static final String MEAL_TABLE_NAME = "meals";
        public static final String CREATE_TABLE_MEALS = "create table " + MEAL_TABLE_NAME
                + "(" + Cols.MEAL_ID + " integer primary key," + Cols.MEAL_NAME + " text,"
                + Cols.MEAL_DATE + " datetime" + ")";

        public static final class Cols {
            public static final String MEAL_ID = "_id";
            public static final String MEAL_NAME = "meal_name";
            public static final String MEAL_DATE = "meal_date";
        }
    }

    public static final class FoodTable {
        public static final String FOOD_TABLE_NAME = "foods";
        public static final String CREATE_TABLE_FOODS = "create table " + FOOD_TABLE_NAME
                + "(" + Cols.FOOD_ID + " integer primary key," + Cols.FOOD_NAME + " text" + ")";

        public static final class Cols {
            public static final String FOOD_ID = "_id";
            public static final String FOOD_NAME = "food_name";
        }
    }

    public static final class MealFoods {
        public static final String MEALFOODS_TABLE_NAME = "meal_foods";
        public static final String CREATE_TABLE_MEALFOODS = "create table "
                + MEALFOODS_TABLE_NAME + "("
                + Cols.MEAL_ID + " integer,"
                + Cols.FOOD_ID + " integer, "
                + "foreign key (" + Cols.MEAL_ID + ") references " + MealTable.MEAL_TABLE_NAME + "(" + MealTable.Cols.MEAL_ID + ") on delete cascade)";

        public static final class Cols {
            public static final String MEAL_ID = "meal_id";
            public static final String FOOD_ID = "food_id";
        }
    }

    public static final class UserSymptoms {
        public static final String USER_SYMPTOMS_TABLE_NAME = "symptom_log";
        public static final String CREATE_TABLE_USERSYMPTOMS = "create table "
                + USER_SYMPTOMS_TABLE_NAME + "("
                + Cols.USER_SYMPTOM_ID + " integer primary key, "
                + Cols.USER_SYMPTOM_DATE + " datetime, "
                + Cols.SYMPTOM_ID + " integer, "
                + Cols.USER_NOTES + " text, "
                + "foreign key (" + Cols.SYMPTOM_ID + ") references " + SymptomsTable.SYMPTOMS_TABLE_NAME + "(" + SymptomsTable.Cols.SYMPTOM_ID + "))";

        public static final class Cols {
            public static final String USER_SYMPTOM_ID = "user_symptom_id";
            public static final String USER_SYMPTOM_DATE = "symptom_date";
            public static final String SYMPTOM_ID = "symptom_id";
            public static final String USER_NOTES = "user_notes";
        }
    }

    public static final class SymptomsTable {
        public static final String SYMPTOMS_TABLE_NAME = "symptoms";
        public static final String CREATE_TABLE_SYMPTOMS = "create table "
                + SYMPTOMS_TABLE_NAME + "("
                + Cols.SYMPTOM_ID + " integer primary key, "
                + Cols.SYMPTOM_NAME + " text" + ")";

        public static final class Cols {
            public static final String SYMPTOM_ID = "symptom_id";
            public static final String SYMPTOM_NAME = "symptom_name";
        }
    }
}
