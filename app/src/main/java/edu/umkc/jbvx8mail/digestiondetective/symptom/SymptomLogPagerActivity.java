package edu.umkc.jbvx8mail.digestiondetective.symptom;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import edu.umkc.jbvx8mail.digestiondetective.DigestionDAO;
import edu.umkc.jbvx8mail.digestiondetective.R;

public class SymptomLogPagerActivity extends AppCompatActivity {

        private static final String EXTRA_LOG_ID = "edu.umkc.jbvx8mail.digestiondetective.log_id";
        private ViewPager viewPager;
        private List<SymptomLog> symptomList;

        public static Intent newIntent(Context packageContext, long logId) {
            Intent intent = new Intent(packageContext, SymptomLogPagerActivity.class);
            intent.putExtra(EXTRA_LOG_ID, logId);
            return intent;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_pager);
            viewPager = (ViewPager) findViewById(R.id.activity_pager_view_pager);
            long logId = (long) getIntent().getSerializableExtra(EXTRA_LOG_ID);

            symptomList = DigestionDAO.get(this).getUserSymptoms();
            if (logId == 0) {
                symptomList.add(new SymptomLog());
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
                @Override
                public Fragment getItem(int position) {
                    SymptomLog log = symptomList.get(position);
                    return SymptomLogFragment.newInstance(log.getLogId());
                }

                @Override
                public int getCount() {
                    return symptomList.size();
                }
            });

            for (int i = 0; i < symptomList.size(); i++) {
                if (symptomList.get(i).getLogId() == logId) {
                    viewPager.setCurrentItem(i);
                    break;
                }
            }

        }
    }


