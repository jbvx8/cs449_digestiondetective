package edu.umkc.jbvx8mail.digestiondetective.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import edu.umkc.jbvx8mail.digestiondetective.database.DetectiveDbSchema.MealTable;

import static edu.umkc.jbvx8mail.digestiondetective.database.DetectiveDbSchema.*;

/**
 * Created by AnonymousII on 2/21/2016.
 */
public class DetectiveDbHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "detectiveDb.db";

    public DetectiveDbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(MealTable.CREATE_TABLE_MEALS);

        db.execSQL(FoodTable.CREATE_TABLE_FOODS);
        db.execSQL(MealFoods.CREATE_TABLE_MEALFOODS);
        db.execSQL(SymptomsTable.CREATE_TABLE_SYMPTOMS);
        db.execSQL(UserSymptoms.CREATE_TABLE_USERSYMPTOMS);
    }

    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.setForeignKeyConstraintsEnabled(true);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + MealTable.MEAL_TABLE_NAME);
        db.execSQL("drop table if exists " + FoodTable.FOOD_TABLE_NAME);
        db.execSQL("drop table if exists " + MealFoods.MEALFOODS_TABLE_NAME);
        db.execSQL("drop table if exists " + SymptomsTable.SYMPTOMS_TABLE_NAME);
        db.execSQL("drop table if exists " + UserSymptoms.USER_SYMPTOMS_TABLE_NAME);
        onCreate(db);
    }



}
