package edu.umkc.jbvx8mail.digestiondetective.symptom;


public class Symptom {
    private long symptomId;

    private String symptomName;


    public Symptom() {

    }

    public Symptom(long id, String name) {
        symptomId = id;
        symptomName = name;

    }

    public long getSymptomId() {
        return symptomId;
    }


    public void setSymptomId(long symptomId) {
        this.symptomId = symptomId;
    }

    public String getSymptomName() {
        return symptomName;
    }

    public void setSymptomName(String symptomName) {
        this.symptomName = symptomName;
    }


}
