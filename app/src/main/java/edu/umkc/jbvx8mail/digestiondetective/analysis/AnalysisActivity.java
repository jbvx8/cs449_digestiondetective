package edu.umkc.jbvx8mail.digestiondetective.analysis;

import android.support.v4.app.Fragment;

import edu.umkc.jbvx8mail.digestiondetective.SingleFragmentActivity;

public class AnalysisActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() { return new AnalysisFragment(); }
}
