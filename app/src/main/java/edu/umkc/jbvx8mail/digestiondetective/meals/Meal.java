package edu.umkc.jbvx8mail.digestiondetective.meals;

import java.io.Serializable;
import java.util.Date;


public class Meal implements Serializable {
    private long mealId;
    private String mealName;
    private Date mealDate;


    public Meal() {
        mealDate = new Date();
    }

    public Meal(long id, String name) {
        mealId = id;
        mealName = name;
        mealDate = new Date();
    }


    public long getMealId() {
        return mealId;
    }


    public void setMealId(long mealId) { this.mealId = mealId; }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public Date getMealDate() {
        return mealDate;
    }

    public void setMealDate(Date mealDate) {
        this.mealDate = mealDate;
    }

    @Override
    public int hashCode() {
        return (int) (mealId ^ (mealId >>> 32));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Meal meal = (Meal) o;

        return mealId == meal.mealId;

    }
}
