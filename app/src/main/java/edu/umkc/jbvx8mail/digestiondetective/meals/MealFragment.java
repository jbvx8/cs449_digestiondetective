package edu.umkc.jbvx8mail.digestiondetective.meals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edu.umkc.jbvx8mail.digestiondetective.DatePickerFragment;
import edu.umkc.jbvx8mail.digestiondetective.DigestionDAO;
import edu.umkc.jbvx8mail.digestiondetective.R;
import edu.umkc.jbvx8mail.digestiondetective.TimePickerFragment;


public class MealFragment extends Fragment {
    private Meal meal;
    final List<Food> foodList = new ArrayList<>();
    private DigestionDAO digestionDao;
    private Button dateButton;
    private Button timeButton;
    private Button okButton;
    private Button cancelButton;
    private EditText mealNameField; //change with dialog
    private CheckBox hasBaggedProduceCheckBox;
    private CheckBox hasBeansCheckBox;
    private CheckBox hasSpicyFoodsCheckBox;
    private CheckBox hasHighFiberCheckBox;
    private CheckBox hasDairyCheckBox; //change when adding categories
    private CheckBox hasSoyCheckBox;
    private CheckBox hasWheatCheckBox;
    private CheckBox hasCaffeineCheckBox;

    private static final String ARG_MEAL_ID = "meal_id";
    private static final String DIALOG_DATE = "DialogDate";
    private static final int REQUEST_DATE = 0;
    private static final String DIALOG_TIME = "DialogTime";
    private static final int REQUEST_TIME = 1;

    public static MealFragment newInstance(long mealId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_MEAL_ID, mealId);
        MealFragment fragment = new MealFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        digestionDao = DigestionDAO.get(getActivity());
        long mealId = (long) getArguments().getSerializable(ARG_MEAL_ID);
        meal = digestionDao.getMeal(mealId);
        if (meal == null) {
            meal = new Meal();
        }
        List<Food> foodList = digestionDao.getAllFoods();
        if (foodList.size() == 0) {
            Food food = new Food();
            food.setFoodName("dairy");
            digestionDao.addFood(food);
            food.setFoodName("wheat");
            digestionDao.addFood(food);
            food.setFoodName("soy");
            digestionDao.addFood(food);
            food.setFoodName("caffeine");
            digestionDao.addFood(food);
            food.setFoodName("bagged produce");
            digestionDao.addFood(food);
            food.setFoodName("beans");
            digestionDao.addFood(food);
            food.setFoodName("spicy foods");
            digestionDao.addFood(food);
            food.setFoodName("high fiber");
            digestionDao.addFood(food);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_meal, container, false);

        // edit text for meal to be changed
        mealNameField = (EditText)v.findViewById(R.id.meal_name);
        if (meal != null) {
            mealNameField.setText(meal.getMealName());
        }
        mealNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                meal.setMealName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dateButton = (Button)v.findViewById(R.id.meal_date);
        updateDate();
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(meal.getMealDate());
                dialog.setTargetFragment(MealFragment.this, REQUEST_DATE);
                dialog.show(manager, DIALOG_DATE);
            }
        });

        timeButton = (Button)v.findViewById(R.id.meal_time);
        updateTime();
        timeButton.setOnClickListener((new View.OnClickListener() {
            @Override
        public void onClick(View view) {
                FragmentManager manager = getFragmentManager();
                TimePickerFragment dialog = TimePickerFragment.newInstance(meal.getMealDate());
                dialog.setTargetFragment(MealFragment.this, REQUEST_TIME);
                dialog.show(manager, DIALOG_TIME);
            }
        }));


        foodList.addAll(digestionDao.getFoodsByMeal(meal.getMealId()));

        hasBaggedProduceCheckBox = makeCheckBox(v, "bagged produce");
        hasBeansCheckBox = makeCheckBox(v, "beans");
        hasCaffeineCheckBox = makeCheckBox(v, "caffeine");
        hasDairyCheckBox = makeCheckBox(v, "dairy");
        hasHighFiberCheckBox = makeCheckBox(v, "high fiber");
        hasSoyCheckBox = makeCheckBox(v, "soy");
        hasSpicyFoodsCheckBox = makeCheckBox(v, "spicy foods");
        hasWheatCheckBox = makeCheckBox(v, "wheat");

        okButton = (Button) v.findViewById(R.id.meal_ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (meal != null) { // assert meal is not null
                    meal.setMealId(digestionDao.updateMeal(meal));
                    digestionDao.deleteMealFoods(meal.getMealId());
                    for (Food food : foodList) {
                        int foodId = digestionDao.getFood(food.getFoodName()).getFoodId();
                        digestionDao.addMealFoods(meal.getMealId(), foodId);
                    }
                }
                if (digestionDao.getFoodsByMeal(meal.getMealId()).size() == 0) {
                    digestionDao.deleteMeal(meal);
                }
                getActivity().onBackPressed();
            }
        });

        cancelButton = (Button) v.findViewById(R.id.meal_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        return v;
    }

    private CheckBox makeCheckBox(View v, final String foodName) {
        CheckBox checkBox = new CheckBox(getContext());
        switch (foodName) {
            case "bagged produce":
                checkBox = (CheckBox) v.findViewById(R.id.has_bagged_produce);
                break;
            case "beans":
                checkBox = (CheckBox) v.findViewById(R.id.has_beans);
                break;
            case "caffeine":
                checkBox = (CheckBox) v.findViewById(R.id.has_caffeine);
                break;
            case "dairy":
                checkBox = (CheckBox) v.findViewById(R.id.has_dairy);
                break;
            case "high fiber":
                checkBox = (CheckBox) v.findViewById(R.id.has_high_fiber);
                break;
            case "soy":
                checkBox = (CheckBox) v.findViewById(R.id.has_soy);
                break;
            case "spicy foods":
                checkBox = (CheckBox) v.findViewById(R.id.has_spicy_foods);
                break;
            case "wheat":
                checkBox = (CheckBox) v.findViewById(R.id.has_wheat);
                break;
        }

        checkBox.setChecked(foodListContains(foodName, foodList));
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Food food = makeFood(foodName);
                updateFoodList(food, isChecked);
            }
        });

        return checkBox;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_meal_delete, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_delete_meal:
                digestionDao.get(getActivity()).deleteMeal(meal);
                meal = null;
        }
        getActivity().finish();
        return true;
    }

    public boolean foodListContains(String foodName, List<Food> foodList) {
        for (int i = 0; i < foodList.size(); i++) {
            if (foodList.get(i).getFoodName().equals(foodName)) {
                return true;
            }
        }
        return false;
    }

    private Food makeFood(String foodName) {
        Food food = new Food();
        food.setFoodName(foodName);
        return food;
    }

    private void updateFoodList(Food food, boolean addFood) {
        if (addFood) {
            foodList.add(food);
        }
        else {
            for (int i = 0; i < foodList.size(); i++) {
                if (foodList.get(i).getFoodName().equals(food.getFoodName())) {
                    foodList.remove(i);
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_DATE) {
            Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            meal.setMealDate(date);
            updateDate();
        }
        if (requestCode == REQUEST_TIME) {
            Date date = (Date) data.getSerializableExtra(TimePickerFragment.EXTRA_TIME);
            meal.setMealDate(date);
            updateTime();
        }
    }

    private void updateDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("EEE, dd MMMMMMM yyyy");
        Date date = meal.getMealDate();
        dateButton.setText(sdfDate.format(date));
    }

    private void updateTime() {
        SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a");
        Date time = meal.getMealDate();
        timeButton.setText(sdfTime.format(time));
    }


}
