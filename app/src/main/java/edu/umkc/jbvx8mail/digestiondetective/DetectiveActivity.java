package edu.umkc.jbvx8mail.digestiondetective;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.UUID;

import edu.umkc.jbvx8mail.digestiondetective.analysis.AnalysisActivity;
import edu.umkc.jbvx8mail.digestiondetective.meals.MealListActivity;
import edu.umkc.jbvx8mail.digestiondetective.symptom.SymptomListActivity;

public class DetectiveActivity extends AppCompatActivity {

    private static final String TAG = "Digestion Detective";
    private static final String EXTRA_MEAL_ID = "edu.umkc.jbvx8mail.digestiondetective.meal_id";
    private Button foodDiaryButton;
    private Button digestiveDiaryButton;
    private Button analysisButton;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment;
            switch (v.getId()) {
                case R.id.food_diary_button:
                    Intent intent = new Intent(DetectiveActivity.this, MealListActivity.class);
                    startActivity(intent);
                    break;
                case R.id.digestion_diary_button:
                    Intent digestionIntent = new Intent(DetectiveActivity.this, SymptomListActivity.class);
                    startActivity(digestionIntent);
                    break;
                case R.id.analysis_button:
                    Intent analysisIntent = new Intent(DetectiveActivity.this, AnalysisActivity.class);
                    startActivity(analysisIntent);
                    break;

            }
        }
    };

    public static Intent newIntent(Context packageContext, UUID mealId) {
        Intent intent = new Intent(packageContext, DetectiveActivity.class);
        intent.putExtra(EXTRA_MEAL_ID, mealId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);

        foodDiaryButton = (Button) findViewById(R.id.food_diary_button);
        digestiveDiaryButton = (Button) findViewById(R.id.digestion_diary_button);
        analysisButton = (Button) findViewById(R.id.analysis_button);

        foodDiaryButton.setOnClickListener(onClickListener);

        digestiveDiaryButton.setOnClickListener(onClickListener);

        analysisButton.setOnClickListener(onClickListener);


        }


}
