package edu.umkc.jbvx8mail.digestiondetective.symptom;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import edu.umkc.jbvx8mail.digestiondetective.BuildConfig;
import edu.umkc.jbvx8mail.digestiondetective.DatePickerFragment;
import edu.umkc.jbvx8mail.digestiondetective.DigestionDAO;
import edu.umkc.jbvx8mail.digestiondetective.R;
import edu.umkc.jbvx8mail.digestiondetective.TimePickerFragment;


public class SymptomLogFragment extends Fragment {
    private SymptomLog symptomLog;
    private DigestionDAO digestionDao;
    private Symptom symptom = new Symptom();
    private Button dateButton;
    private Button timeButton;
    private Button okButton;
    private Button cancelButton;
    private EditText symptomNotesField; //change with dialog
    private RadioButton heartburnButton, abdominalButton, nauseaButton, hivesButton, constipationButton, looseButton, hardButton;


    private static final String ARG_SYMPTOM_LOG_ID = "symptom_id";
    private static final String DIALOG_RECENT_MEALS = "DialogRecentMeals";
    private static final String DIALOG_DATE = "DialogDate";
    private static final int REQUEST_DATE = 0;
    private static final String DIALOG_TIME = "DialogTime";
    private static final int REQUEST_TIME = 1;

    public static SymptomLogFragment newInstance(long symptomLogId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_SYMPTOM_LOG_ID, symptomLogId);
        SymptomLogFragment fragment = new SymptomLogFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        digestionDao = DigestionDAO.get(getActivity());
        assert digestionDao != null;
        long logId = (long) getArguments().getSerializable(ARG_SYMPTOM_LOG_ID);
        symptomLog = digestionDao.getUserSymptom(logId);
        if (symptomLog == null) {
            symptomLog = new SymptomLog();
        }
        if (symptomLog != null) {
            symptom = symptomLog.getSymptom();
        }

        int count = digestionDao.getSymptomCount();
        if (count == 0) {
            Symptom symptom = new Symptom();
            symptom.setSymptomName("heartburn");
            digestionDao.addSymptom(symptom);
            symptom.setSymptomName("nausea");
            digestionDao.addSymptom(symptom);
            symptom.setSymptomName("hives");
            digestionDao.addSymptom(symptom);
            symptom.setSymptomName("abdominal pain");
            digestionDao.addSymptom(symptom);
            symptom.setSymptomName("constipation");
            digestionDao.addSymptom(symptom);
            symptom.setSymptomName("loose stool");
            digestionDao.addSymptom(symptom);
            symptom.setSymptomName("hard stool");
            digestionDao.addSymptom(symptom);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_symptom, container, false);

        heartburnButton = (RadioButton) v.findViewById(R.id.radioHeartburn);
        heartburnButton.setOnClickListener(radioListener);

        abdominalButton = (RadioButton) v.findViewById(R.id.radioAbdominalPain);
        abdominalButton.setOnClickListener(radioListener);

        hivesButton = (RadioButton) v.findViewById(R.id.radioHives);
        hivesButton.setOnClickListener(radioListener);

        constipationButton = (RadioButton) v.findViewById(R.id.radioConstipation);
        constipationButton.setOnClickListener(radioListener);

        nauseaButton = (RadioButton) v.findViewById(R.id.radioNausea);
        nauseaButton.setOnClickListener(radioListener);

        looseButton = (RadioButton) v.findViewById(R.id.radioLoose);
        looseButton.setOnClickListener(radioListener);

        hardButton = (RadioButton) v.findViewById(R.id.radioHard);
        hardButton.setOnClickListener(radioListener);


        if (symptomLog != null && symptomLog.getSymptom().getSymptomId() != 0) {
            setCheckedRadioButton();
        }


        symptomNotesField = (EditText)v.findViewById(R.id.symptom_notes);
        symptomNotesField.setText(symptomLog.getSymptomNotes());
        symptomNotesField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                symptomLog.setSymptomNotes(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        dateButton = (Button)v.findViewById(R.id.symptom_date);
        updateDate();
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(symptomLog.getSymptomDate());
                dialog.setTargetFragment(SymptomLogFragment.this, REQUEST_DATE);
                dialog.show(manager, DIALOG_DATE);
            }
        });

        timeButton = (Button)v.findViewById(R.id.symptom_time);
        updateTime();
        timeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getFragmentManager();
                TimePickerFragment dialog = TimePickerFragment.newInstance(symptomLog.getSymptomDate());
                dialog.setTargetFragment(SymptomLogFragment.this, REQUEST_TIME);
                dialog.show(manager, DIALOG_TIME);
            }
        });

        okButton = (Button)v.findViewById(R.id.symptom_ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (symptomLog != null) {
                    Symptom tempSymptom = digestionDao.getSymptom(symptom.getSymptomName());
                    if (tempSymptom != null) {
                        long symptomId = tempSymptom.getSymptomId();
                        if (BuildConfig.DEBUG && symptomId == 0) {throw new AssertionError("symptomId is 0");}
                        symptom.setSymptomId(symptomId);
                        symptomLog.setSymptom(symptom);
                        digestionDao.ensureLog(symptomLog);
                    }
                }
                getActivity().onBackPressed();
            }
        });

        cancelButton = (Button)v.findViewById(R.id.symptom_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        return v;
    }



    public View.OnClickListener radioListener = new View.OnClickListener() {
        public void onClick(View view) {
            boolean checked = ((RadioButton) view).isChecked();
            switch (view.getId()) {
                case R.id.radioHeartburn:
                    if (checked) {
                        symptom.setSymptomName("heartburn");
                    }
                    break;
                case R.id.radioAbdominalPain:
                    if (checked) {
                        symptom.setSymptomName("abdominal pain");
                    }
                    break;
                case R.id.radioHives:
                    if (checked) {
                        symptom.setSymptomName("hives");
                    }
                    break;
                case R.id.radioNausea:
                    if (checked) {
                        symptom.setSymptomName("nausea");
                    }
                    break;
                case R.id.radioConstipation:
                    if (checked) {
                        symptom.setSymptomName("constipation");
                    }
                    break;
                case R.id.radioLoose:
                    if (checked) {
                        symptom.setSymptomName("loose stool");
                    }
                    break;
                case R.id.radioHard:
                    if (checked) {
                        symptom.setSymptomName("hard stool");
                    }
                    break;

            }
        }
    };

    public void setCheckedRadioButton() {
        switch (symptom.getSymptomName()) {
            case "heartburn":
                heartburnButton.setChecked(true); break;
            case "nausea":
                nauseaButton.setChecked(true); break;
            case "hives":
                hivesButton.setChecked(true); break;
            case "abdominal pain":
                abdominalButton.setChecked(true); break;
            case "constipation":
                constipationButton.setChecked(true); break;
            case "loose stool":
                looseButton.setChecked(true); break;
            case "hard stool":
                hardButton.setChecked(true); break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (BuildConfig.DEBUG && symptomLog == null) {throw new AssertionError("symptomLog is null");}
        if (BuildConfig.DEBUG && symptomLog.getSymptom() == null) {throw new AssertionError("symptom is null");}

        if (symptomLog.getSymptom().getSymptomName() != null) {
            inflater.inflate(R.menu.fragment_symptom_log_delete, menu);
            inflater.inflate(R.menu.fragment_symptom_recent_meals, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_item_delete_user_symptom:
                digestionDao.get(getActivity()).deleteSymptomLog(symptomLog);
                symptomLog = null;
                return true;
            case R.id.menu_item_recent_meals:
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.dialog_recent_meals, null);
                AlertDialog dialog = new AlertDialog.Builder(getActivity()).setTitle(R.string.dialog_title_recent_meals)
                        .setView(v)
                        .setPositiveButton(android.R.string.ok, null)
                        .create();

                dialog.show();
                updateFoods(dialog);
                return true;
        }
        getActivity().finish();
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_DATE) {
            Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            if (BuildConfig.DEBUG && symptomLog == null) {throw new AssertionError("symptomLog is null");}
            symptomLog.setSymptomDate(date);
            updateDate();
        }
        if (requestCode == REQUEST_TIME) {
            Date date = (Date) data.getSerializableExtra(TimePickerFragment.EXTRA_TIME);
            if (BuildConfig.DEBUG && symptomLog == null) {throw new AssertionError("symptomLog is null");}
            symptomLog.setSymptomDate(date);
            updateTime();
        }
    }

    private void updateFoods(Dialog dialog) {
        if (BuildConfig.DEBUG && symptomLog == null) {throw new AssertionError("symptomLog is null");}
        Map<String, Integer> foodMap = digestionDao.getFoodsByDate(symptomLog.getSymptomDate());
        for(Map.Entry<String, Integer> entry : foodMap.entrySet()) {
            int count = entry.getValue();
            switch (entry.getKey()) {
                case "soy":
                    TextView soy_count = (TextView) dialog.findViewById(R.id.soy_value);
                    soy_count.setText(Integer.toString(count));
                    break;
                case "dairy":
                    TextView dairy_count = (TextView) dialog.findViewById(R.id.dairy_value);
                    dairy_count.setText(Integer.toString(count));
                    break;
                case "wheat":
                    TextView wheat_count = (TextView) dialog.findViewById(R.id.wheat_value1);
                    wheat_count.setText(Integer.toString(count));
                    break;
                case "caffeine":
                    TextView caffeine_count = (TextView) dialog.findViewById(R.id.caffeine_value);
                    caffeine_count.setText(Integer.toString(count));
                    break;
            }
        }

    }

    private void updateDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("EEE, dd MMMMMMM yyyy");
        if (BuildConfig.DEBUG && symptomLog == null) {throw new AssertionError("symptomLog is null");}
        Date date = symptomLog.getSymptomDate();
        dateButton.setText(sdfDate.format(date));
    }

    private void updateTime() {
        SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a");
        if (BuildConfig.DEBUG && symptomLog == null) {throw new AssertionError("symptomLog is null");}
        Date time = symptomLog.getSymptomDate();
        timeButton.setText(sdfTime.format(time));
    }

}
