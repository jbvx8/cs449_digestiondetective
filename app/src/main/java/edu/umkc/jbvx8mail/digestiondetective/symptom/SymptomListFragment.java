package edu.umkc.jbvx8mail.digestiondetective.symptom;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import edu.umkc.jbvx8mail.digestiondetective.BuildConfig;
import edu.umkc.jbvx8mail.digestiondetective.DigestionDAO;
import edu.umkc.jbvx8mail.digestiondetective.R;


public class SymptomListFragment extends Fragment {
    private RecyclerView recyclerView;
    private SymptomAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        if (BuildConfig.DEBUG && view == null) {throw new AssertionError("view is null");}
        recyclerView = (RecyclerView) view
                .findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager((new LinearLayoutManager(getActivity())));

        updateUI();

        return view;
    }

    public  void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_symptom_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_symptom:
                SymptomLog log = new SymptomLog();
                log.setSymptom(new Symptom());
                Intent intent = SymptomLogPagerActivity.newIntent(getActivity(), 0);
                startActivity(intent);
                return true;
            default:return super.onOptionsItemSelected(item);
        }

    }

    private void updateUI() {
        DigestionDAO digestionDAO = DigestionDAO.get(getActivity());
        assert digestionDAO != null;
        List<SymptomLog> logList = digestionDAO.getUserSymptoms();
        if (adapter == null)
        {
            adapter = new SymptomAdapter(logList);
           recyclerView.setAdapter(adapter);
        }
        else {
            adapter.setLogList(logList);
            adapter.notifyDataSetChanged();
        }

    }

    private class LogHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView symptomNameTextView;
        private TextView symptomDateTextView;
        private TextView symptomNotesTextView;



        private SymptomLog log;
        public LogHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            symptomNameTextView = (TextView) itemView.findViewById(R.id.list_item_symptom_name_text_view);
            symptomDateTextView = (TextView) itemView.findViewById(R.id.list_item_symptom_date_text_view);
            symptomNotesTextView = (TextView) itemView.findViewById(R.id.list_item_symptom_notes_text_view);
        }

        @Override
        public void onClick(View v) {
            Intent intent = SymptomLogPagerActivity.newIntent(getActivity(), log.getLogId());
            startActivity(intent);
        }

        public void bindLog(SymptomLog sl) {
            log = sl;
            symptomNameTextView.setText(log.getSymptom().getSymptomName());
            symptomDateTextView.setText(log.getSymptomDate().toString());
            symptomNotesTextView.setText(log.getSymptomNotes());
        }

    }



    private class SymptomAdapter extends RecyclerView.Adapter<LogHolder> {
        private List<SymptomLog> logList;
        public SymptomAdapter(List<SymptomLog> userSymptoms) {
            logList = userSymptoms;
        }

        @Override
        public LogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater
                    .inflate(R.layout.list_item_symptoms, parent, false);
            return new LogHolder(view);
        }

        @Override
        public void onBindViewHolder(LogHolder holder, int position) {
            SymptomLog log = logList.get(position);
            holder.bindLog(log);
        }

        @Override
        public int getItemCount() {
            return logList.size();
        }

        public void setLogList(List<SymptomLog> userSymptoms) {
            logList = userSymptoms;
        }
    }
}

